angular.module('aiwApp').controller('NewsRelatedController', function($scope,$stateParams,NewsComment,$state,$http,$interval,ApiUrl){
    $http.get(ApiUrl+"news/"+$stateParams.slug)
        .then(function(response) {
            $scope.relatedNews=response.data.data[0].related_news;
        });
});