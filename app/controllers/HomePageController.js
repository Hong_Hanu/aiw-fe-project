angular.module('aiwApp').controller('HomePageController', function ($scope,Category,$stateParams,$state,SearchCategory,$q) {
    $scope.news ={};
    Category.query().$promise.then(function(response){
    	var length = response.data.length;
    	$scope.nameCat= response.data;
    	var promises =[];
    	var i=0;
    	for(i;i<length;i++){
    		var cat = response.data[i].name;
    		promises.push(SearchCategory.query(cat));
    	}
    	return $q.all(promises);
    }).then(function(results){
    	
    	var length = $scope.nameCat.length;
    	var i=0;
    	for(i;i<length;i++){
    		var nameCat = $scope.nameCat[i].name;
    		if(results[i].data.data.length!=0){
    			$scope.news[nameCat]=results[i].data.data;
    		}	
    	}
    	// console.log(results);
    });
});