angular.module('aiwApp').controller('NewsDetailController', function($scope,$stateParams,NewsComment,$state,$http,$interval,ApiUrl){

    // Show list Comment
    $scope.commentData={};
    $http.get(ApiUrl+"news/"+$stateParams.slug).then(function(response) {
        $scope.news=response.data;
        if(typeof(EventSource) !== "undefined") {
            var source = new EventSource(ApiUrl+"newscomment/"+$stateParams.slug+"/update");
            source.onmessage = function(event) {
                var x = JSON.parse(event.data);
                if(x!=undefined){
                    $scope.news.data[0].comments=x;
                    $scope.$digest();
                }else{
                    console.log("error when read data");
                }
            };
        } else {
            console.log("error");
        }
    });

    // Get data submit comment
    $scope.submitContent = function(){
        console.log($scope.commentData);
        NewsComment.save($scope.commentData).success(function(data){
            $scope.commentData.content="";
            var x = $stateParams.slug;
            $http.get(ApiUrl+"news/"+x)
                .then(function(response) {
                    $scope.news=response.data;
                });
        })
            .error(function(){
                console.log("error");
            });

    };

});