/**
 * Show data category
 * @param obj $scope, Category - data from CategoryService.js
 *
 */
angular.module('aiwApp').controller('CategoryController', function ($scope, Category) {
    // Use $scope to load data categories from controller to view
    $scope.categories=Category.query();
});