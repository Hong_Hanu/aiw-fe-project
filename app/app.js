var aiw = angular.module('aiwApp', ['ui.router', 'ngResource','ngSanitize','ngAnimate','ngTouch','ui.bootstrap']);
aiw.config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
        .state('homepage', {
            url: '/',
            views: {
                "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main": {
                    templateUrl: 'homepage.html',
                    controller: "HomePageController"
                },
                "search":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }
            
            
        })
        .state('news', {
            url: '/news',
            views: {
                "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main": {
                    templateUrl: 'news.html',
                    controller: "NewsController"
                },
                "search":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }
        })
        .state('news.pagination',{
            url: '/{page}',
            views: {
                "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main@": {
                    templateUrl: 'news.html',
                    controller: "NewsController"
                },
                "search@news.pagination":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }
        })
        .state('newsDetail',{
            url:'/article/:slug',
            views: {
                "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main": {
                    templateUrl:'post.html',
                    controller: "NewsDetailController"
                },
                "related-news@newsDetail":{
                    templateUrl:'related-news.html',
                    controller: ""
                },
                "search":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }
            
            
        })
        .state('multimedia', {
            url: '/multimedia',
            templateUrl: 'index.html',
            controller: function($scope,Multimedia){
                $scope.news=Multimedia.query();
            }
        })
        .state('searchResult',{
            url:'/search/:title',
            views: {
                "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main@": {
                    templateUrl: 'result-search.html',
                    controller: "SearchController"
                },
                "search":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }
        })
        .state('searchResult.pagination',{
            url :'/{page}',
            views: {
                "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main@": {
                    templateUrl: 'result-search.html',
                    controller: "SearchController"
                },
                "search@searchResult.pagination":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }

        })
        .state('searchCategory',{
            url:'/category/:name',
            views:{
                 "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main@": {
                    templateUrl: 'result-search.html',
                    controller: "SearchCategoryController"
                },
                "search":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }

        })
        .state('searchCategory.pagination',{
            url :'/{page}',
            views: {
                "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main@": {
                    templateUrl: 'result-search.html',
                    controller: "SearchCategoryController"
                },
                "search@searchCategory.pagination":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }

        })
        .state('tag',{
            url :'/tag/{name}',
            views: {
                "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main@": {
                    templateUrl: 'result-search.html',
                    controller: "TagController"
                },
                "search":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }

        })
        .state('tag.pagination',{
            url :'/{page}',
            views: {
                "menu":{
                    templateUrl: 'menu.html',
                    controller: "CategoryController"
                },
                "main@": {
                    templateUrl: 'result-search.html',
                    controller: "TagController"
                },
                "search@tag.pagination":{
                    templateUrl:'search.html',
                    controller: 'SearchFormController'
                }
            }

        })
    $urlRouterProvider.otherwise('/');
})
.constant('ApiUrl', 'http://server.aiw.io/');
aiw.config(function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['**']);
});

