/**
 * Get data from api category
 * @param string ApiUrl
 * @return $resource
 *
 */
angular.module('aiwApp').factory('Category', function ($resource,ApiUrl) {
    return $resource(ApiUrl+'category',{},{
        'query':  {mehod:'GET',isArray: false}
    });
});