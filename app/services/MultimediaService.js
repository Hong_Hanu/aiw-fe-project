angular.module('aiwApp').factory('Multimedia', function ($resource) {
    return $resource('http://fit.io/aiw-2016/public/multimedia?page=:page',{page: '@_page'},{
        'get':    {method:'GET'},
    });
});