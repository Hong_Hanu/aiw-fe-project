angular.module('aiwApp')
.factory('Tag', function ($http,ApiUrl,$stateParams) {
     return{
     	query:function(){
            
     		return $http({
     			method: 'GET',
     			url : ApiUrl+'tag/'+$stateParams.name+'?page='+($stateParams.page==undefined?1:$stateParams.page),
     			headers:{'Content-Type' : 'application/json' },
     		});
     	}
     }
})
