angular.module('aiwApp')
.factory('NewsComment', function ($http,ApiUrl) {
     return{
     	save:function(commentData){
     		return $http({
     			method: 'POST',
     			url : ApiUrl+'newscomment',
     			headers:{'Content-Type' : 'application/json' },
     			data: commentData
     		});
     	}
     }
})
