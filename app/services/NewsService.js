/**
 * Get data from api news
 * @param string ApiUrl
 * @return $resource
 *
 */
angular.module('aiwApp')
.factory('News', function ($resource,ApiUrl) {
     return $resource(ApiUrl+'news?page=:page',{page: '@_page'},{
        'get':    {method:'GET'},
    })
})
.factory('NewsDetail',function($resource){
	return $resource(ApiUrl+'news/:slug',{slug: '@_slug'},{
        'get':    {method:'GET'},
    })
});