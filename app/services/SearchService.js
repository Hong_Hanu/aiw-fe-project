angular.module('aiwApp')
.factory('Search', function ($http,ApiUrl,$stateParams) {
     return{
     	query:function(){
     		return $http({
     			method: 'GET',
     			url : ApiUrl+'search/'+$stateParams.title+'?page='+($stateParams.page==undefined?1:$stateParams.page),
     			headers:{'Content-Type' : 'application/json' },
     		});
     	}
     }
})
